import java.util.Arrays;
/**
 * Tests TicTacToeGame
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class GameTester
{
    public static void main(String[] args)
    {
        int[][] win1 = {{ 1, 2, 1},
                         {2, 1, 2},
                         {2, 0, 1}
                        };
        
        TicTacToeGame game1 = new TicTacToeGame(win1);
        System.out.println(game1.winner());
        System.out.println("Expected: 0");
        System.out.println(game1.getBoard());
        System.out.println("Expected: null");
        System.out.println(Arrays.toString(game1.counts()));
        System.out.println("Expected: [0, 0, 0]");
        
        
        
        
    }
}
