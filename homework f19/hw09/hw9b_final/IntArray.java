//SOLUTION
/**
 * Processor for int array
 */
public class IntArray
{
    private int[] integers;

    /**
     * Constructs an IntArray with the given array
     * @param array the array to use in this IntArray object
     */
    public IntArray(int[] array)
    {
        integers = array;
    }

    /**
     * Gets the smallest integer in the array
     * @return the smallest integer in the array or 
     * Integer.MIN_VALUE if the array is empty
     */
    public int smallest()
    {
        if (integers.length == 0)
        {
            return Integer.MIN_VALUE;
        }

        int smallest = integers[0];
        for(int n : integers)
        {
            if (n < smallest)
            {
                smallest = n;
            }
        }
        return smallest;
    }

    /**
     * Gets a String containing  all the negative numbers in the 
     * array separated by a comma and then a space(", ")
     * @return a String containing  all the negative numbers in the 
     * array separated by a comma and then a space(", ")
     */
    public String negatives()
    {
        String display = "";
        int count = 0;
        
        for (int i = 0; i < integers.length; i++)
        {            
            if (integers[i] < 0)
            {
                count++;
                display = display + integers[i];
                if (count != negativeCount()) //this is not the last
                {
                    display = display + ", ";
                }

            }
        }
        return display;
    }

    /**
     * Gets the number of integers less than or equal to 0
     * @return the number of integers less than or equal to 0
     */
    public int negativeCount()
    {
        int count = 0;
        for(int n : integers)
        {
            if (n < 0)
            {
                count++;
            }
        }
        return count;
    }

}
