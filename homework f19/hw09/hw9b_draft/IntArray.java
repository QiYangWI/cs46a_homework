//SOLUTION
/**
 * Processor for int array
 */
public class IntArray
{
    private int[] integers;

    /**
     * Constructs an IntArray with the given array
     * @param array the array to use in this IntArray object
     */
    public IntArray(int[] array)
    {
        integers = array;
    }

    /**
     * Gets the smallest integer in the array
     * @return the smallest integer in the array or 
     * Integer.MIN_VALUE if the array is empty
     */
    public int smallest()
    {
        if (integers.length == 0)
        {
            return Integer.MIN_VALUE;
        }
        
        int smallest = integers[0];
        for(int n : integers)
        {
            if (n < smallest)
            {
                smallest = n;
            }
        }
        return smallest;
    }
}
