
/**
 * Tests the EllipseManager class.
 *
 * @author Kathleen O'Brien
 */
public class EllipseManagerArrayTester
{
    public static void main(String[] args)
    {
        Ellipse[] array = {
            new Ellipse(0, 0, 45, 90), 
            new Ellipse(0, 0, 100, 50),
            new Ellipse(0, 0, 200, 100), 
            new Ellipse(0, 0, 50, 40)  
            };
            
        EllipseManagerArray manager = new EllipseManagerArray(array);        
        
        //test largest
        System.out.println("Largest: " + manager.largest());
        System.out.println("Expected: Ellipse[x=0,y=0,width=200,height=100]");
        
        Ellipse[] array2 = {
            new Ellipse(0, 0, 45, 90), 
            new Ellipse(0, 0, 100, 50),
            new Ellipse(0, 0, 200, 100), 
            new Ellipse(0, 0, 50, 40),
            new Ellipse(0, 0, 100, 200)
            };
            
        EllipseManagerArray manager2 = new EllipseManagerArray(array2);        
        System.out.println("Largest: " + manager2.largest());
        System.out.println("Expected: Ellipse[x=0,y=0,width=200,height=100]");
        
        //test exchange
        manager2.exchange(0, 4);
        System.out.println(manager2);
        System.out.println("Expexcted: [Ellipse[x=0,y=0,width=100,height=200], Ellipse[x=0,y=0,width=100,height=50], Ellipse[x=0,y=0,width=200,height=100], Ellipse[x=0,y=0,width=50,height=40], Ellipse[x=0,y=0,width=45,height=90]]");
        
        //exchange bad index. Should have no effect
        manager2.exchange(-1, 1);
        manager2.exchange(2, -1);
        manager2.exchange(1, 5);
        manager2.exchange(5, 2);
        System.out.println(manager2);
        System.out.println("Expexcted: [Ellipse[x=0,y=0,width=100,height=200], Ellipse[x=0,y=0,width=100,height=50], Ellipse[x=0,y=0,width=200,height=100], Ellipse[x=0,y=0,width=50,height=40], Ellipse[x=0,y=0,width=45,height=90]]");
        
        //test with empty object
        array = new Ellipse[0];
        manager = new EllipseManagerArray(array);
        manager.exchange(1, 3);
        System.out.println(manager);
        System.out.println("Expected: []");
        
        System.out.println("Largest: " + manager.largest());
        System.out.println("Expected: null");      
    }
}
