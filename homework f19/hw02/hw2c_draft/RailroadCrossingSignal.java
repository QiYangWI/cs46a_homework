//SOLUTION
/**
 * Draws a simulation of a railroad crossing signal
 * @author your name
 */
public class RailroadCrossingSignal
{
    public static void main(String[] args)
    {
//HIDE
        Ellipse leftCircle = new Ellipse(50, 30, 40, 40);
        leftCircle.setColor(Color.RED);
        leftCircle.fill();
        
//SHOW
    }
}
