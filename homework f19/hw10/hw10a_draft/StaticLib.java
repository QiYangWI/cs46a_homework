//SOLUTION
import java.util.ArrayList;
/**
 * A Utility class containing static methods
 * for working with arrays and array lists
 */
public class StaticLib
{
    /**
     * Gets the average value in the array
     * @param numbers the array to find the average value of
     * @return the average value in the array or Double.NEGATIVE_INFINITY if 
     * the array is empty
     */
    public static double max(double[] numbers)
    {
        if (numbers.length == 0)
        {
            return Double.NEGATIVE_INFINITY;
        }
        
        double max = numbers[0];
        for (double n : numbers)
        {
            if (n > max)
            {
                max = n;
            }
        }
        return max;
    }
}
