
/**
 * Tester for the Wildebeest class
 */
public class WildebeestTester
{
    public static void main(String[] args)
    {
        //Are the constants defined?
        System.out.println(Wildebeest.NOT_HUNGRY + " " + Wildebeest.VERY_HUNGRY);
        System.out.println("Expected: 1 4");
        
        
        Wildebeest wildebeest = new Wildebeest();
        System.out.println("state: " + wildebeest.getState());
        System.out.println("Expected: 4");
        
        wildebeest.run();
        System.out.println("state: " + wildebeest.getState()
            + " " + wildebeest.getHungerLevel());
        System.out.println("Expected: 4 null");
        
        wildebeest.seeFood();
        System.out.println("state: " + wildebeest.getState()
            + " " + wildebeest.getHungerLevel());
        System.out.println("Expected: 4 null");
        
        wildebeest.seeFood();
        System.out.println("state: "  + " " + wildebeest.getHungerLevel());
        System.out.println("Expected: null");
        
        
    }
}
