//SOLUTION
/**
 * Draws 10 concenric circles.
 *
 * @author KOPBrien
 * 
 */
public class CircleViewer
{
    public static void main(String[] args)
    {
        Ellipse circle = new Ellipse(200 - 10, 250 - 10, 2 * 10, 2 * 10);
        circle.draw();
    }
}
