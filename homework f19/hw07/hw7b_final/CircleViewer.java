//SOLUTION
/**
 * Draws 10 concenric circles.
 *
 * @author KOBrien
 * 
 */
public class CircleViewer
{
    public static void main(String[] args)
    {
        final int CENTER_X = 200;
        final int CENTER_Y = 250;
        final int FIRST_RADIUS = 10;
        final int RADIUS_INCREMENT = 10;
        
        int x = CENTER_X;
        int y = CENTER_Y;
        int radius = FIRST_RADIUS;
        for (int i = 1; i <= 10; i++)
        {
            Ellipse circle = new Ellipse(x - radius, y - radius, 2 * radius, 2 * radius);
            circle.draw();
            radius = radius + RADIUS_INCREMENT;
        }
    }
}
