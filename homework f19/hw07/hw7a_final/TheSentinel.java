//SOLUTION
//HIDE
import java.util.Scanner;
/**
 * Get a set of integers and print some infor about them
 */
public class TheSentinel
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        double sum = 0;
        double min = 0;
        int belowSeaLevel = 0;
        int count = 0;
        
        System.out.print("Enter an elevation or Q to quit: ");
        while (scan.hasNextDouble())
        {           
            double elevation = scan.nextDouble();
            
            if (elevation < 0)
            {
                belowSeaLevel++;
            }
            
            if (count == 0)
            {
                min = elevation;
                
            }
            else if(elevation < min)
            {
                min = elevation;
            }

            sum = sum + elevation;        
            count++;
            
            System.out.print("Enter an integer or Q to quit: ");
        }
        
        if (count == 0)
        {
            System.out.println("no input");
        }
        else
        {
            System.out.println(belowSeaLevel);
            System.out.println(min);
            System.out.println(sum / count);           
        }
    }
}
