//SOLUTION
//HIDE
import java.util.Scanner;
/**
 * Get a set of integers and print some infor about them
 */
public class TheSentinel
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);      
        int belowSeaLevel = 0;
        int count = 0;
        
        System.out.print("Enter an elevation or Q to quit: ");
        while (scan.hasNextDouble())
        {           
            double elevation = scan.nextDouble();
            
            if (elevation < 0)
            {
                belowSeaLevel++;
            }                 
            
            count++;
            
            System.out.print("Enter an integer or Q to quit: ");
        }
        
        if (count == 0)
        {
            System.out.println("no input");
        }
        else
        {
            System.out.println(belowSeaLevel);
         
        }
    }
}
