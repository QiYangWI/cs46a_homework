//SOLUTION
/**
 * Models an animal that can not eat or move too much.
 * 
 */
public class BetterAnimal extends Animal
{
    private int maxEnergy;
    
     /**
     * Constructs an BetterAnimal with a 
     * maximum energy level
     * @param max the maximum energy level
     */
    public BetterAnimal(int max)
    {
        super(); 
        maxEnergy = max;
    }
    
        /**
     * The BetterAnimal eats and increases energy
     * @param amountToEat the amount eaten. 
     * Energy only changes if amount is > 0
     * Energy can never be greater than the maximum
     */
    public void eat(int amountToEat)
    {
        if (amountToEat > 0)
        {
            if(getEnergy() + amountToEat > maxEnergy)
                super.eat(maxEnergy-getEnergy());
            else
                super.eat(amountToEat);

        }
    }
    
     /**
     * The BetterAnimal moves and decreases energy
     * @param amountToMove the amount moved. 
     * Energy only changes if amountToMove is > 0
     * and can not decrease to less than 0.
     */
    public void move(int amountToMove)
    {
        if (amountToMove > 0)
        {
            if(getEnergy() < amountToMove)
            {
                super.move(getEnergy());
            }
            else
            {
                super.move(amountToMove);
            }
        }
    }    
}
