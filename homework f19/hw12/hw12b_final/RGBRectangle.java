//SOLUTION
/**
 * Describes a Rectangle that can be red, 
 * green, blue, or gray
 */
public class RGBRectangle extends Rectangle
{
    private String rgbColor;

    /**
     * Constructor for objects of class rgbRectangle
     * @param x the x xoordinate
     * @param y the y coordinate
     * @param w the width
     * @param h the height
     */
    public RGBRectangle(int x, int y, int w, int h)
    {
        super(x, y, w, h);
        setRGBColor("default");
    }
    
     /**
     * Constructor for objects of class RGBRectangle
     * @param x the x xoordinate
     * @param y the y coordinate
     * @param w the width
     * @param h the height
     * @param color the color of this rectangle
     */
    public RGBRectangle(int x, int y, int w, 
    int h, String color)
    {
        super(x, y, w, h);
        setRGBColor(color);
    } 

    /**
     * Gets the rgb color of this rectangle
     * @return the rgb color of this rectangle
     */
    public String getRGBColor()
    {
        return rgbColor;
    }

    /**
     * Sets the rgb color of this rectangle
     * @param newColor the rgb color
     */
    public void setRGBColor(String newColor)
    {
        rgbColor = newColor;
        if (newColor.equals("red"))
        {
            super.setColor(Color.RED);
        }
        else if (newColor.equals("blue"))
        {
            super.setColor(Color.BLUE);
        }
        else if (newColor.equals("green"))
        {
            super.setColor(Color.GREEN);
        }
        else
        {
            rgbColor = "gray";
            super.setColor(Color.GRAY);
        }
    }
    
    /**
     * Does not change the color
     * @param ignored color is not changed
     */
    public void setColor(Color ignored)
    {
    }    
}
