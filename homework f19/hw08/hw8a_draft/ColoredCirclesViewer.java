
/**
 * Test the ColoredCircles class.
 *
 * @author KOBrien
 * 
 */
public class ColoredCirclesViewer
{
    public static void main(String[] args)
    {
        ColoredCircles grid1 = new ColoredCircles(0, 10, 10);
        grid1.draw();
        
        ColoredCircles grid2 = new ColoredCircles(30, 120,20);
        grid2.draw();
        
    }
}
