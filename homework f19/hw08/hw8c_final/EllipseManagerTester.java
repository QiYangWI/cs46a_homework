
/**
 * Tests the EllipseManager class.
 *
 * @author Kathleen O'Brien
 */
public class EllipseManagerTester
{
    public static void main(String[] args)
    {
        EllipseManager manager = new EllipseManager();
        manager.add(new Ellipse(0, 0, 45, 90)); 
        manager.add(new Ellipse(0, 0, 100, 50)); 
        manager.add(new Ellipse(0, 0, 200, 100)); 
        manager.add(new Ellipse(0, 0, 50, 40));  
         
        
        //test largest
        System.out.println("Largest: " + manager.largest());
        System.out.println("Expected: Ellipse[x=0,y=0,width=200,height=100]");
        
        manager.add(new Ellipse(0, 0, 100, 200));    
        System.out.println("Largest: " + manager.largest());
        System.out.println("Expected: Ellipse[x=0,y=0,width=200,height=100]");
        
        //test exchange
        manager.exchange(0, 4);
        System.out.println(manager);
        System.out.println("Expexcted: [Ellipse[x=0,y=0,width=100,height=200], Ellipse[x=0,y=0,width=100,height=50], Ellipse[x=0,y=0,width=200,height=100], Ellipse[x=0,y=0,width=50,height=40], Ellipse[x=0,y=0,width=45,height=90]]");
        
        //exchange bad index. Should have no effect
        manager.exchange(-1, 1);
        manager.exchange(2, -1);
        manager.exchange(1, 5);
        manager.exchange(5, 2);
        System.out.println(manager);
        System.out.println("Expexcted: [Ellipse[x=0,y=0,width=100,height=200], Ellipse[x=0,y=0,width=100,height=50], Ellipse[x=0,y=0,width=200,height=100], Ellipse[x=0,y=0,width=50,height=40], Ellipse[x=0,y=0,width=45,height=90]]");
        
        //test with empty object
        manager = new EllipseManager();
        manager.exchange(1, 3);
        System.out.println(manager);
        System.out.println("Expected: []");
        
        System.out.println("Largest: " + manager.largest());
        System.out.println("Expected: null");      
    }
}
