//SOLUTION
//SHOW
import java.util.ArrayList;
/**
 * Manages a cllection of Ellipse objects
 *
 */
public class EllipseManager
{
    //add your constructor and methods
    
//HIDE
    private ArrayList<Ellipse> list;

    /**
     * Constructs an empty EllipseManager
     */
    public EllipseManager()
    {
        list = new ArrayList<Ellipse>();
    }

    /**
     * Adds the Ellipse to this EllipseManager
     * @param be the Ellipse to add
     */
    public void add(Ellipse e)
    {
        list.add(e);
    }
    
     /**
     * Swaps the elements at the given indices if they are both in bounds
     * @param index1 the first index to swap
     * @param index2 the second index
     */
    public void exchange(int index1, int index2)
    {
        if (index1 >= 0 && index2 >= 0 
        && index1 < (list.size()) 
        && index2 < (list.size()))
        {
            Ellipse temp = list.get(index1);
            list.set(index1, list.get(index2));
            list.set(index2, temp);       
        }
    }
    
     /**
     * Gets the Ellipse with the lowest price. 
     * @return the Ellipse with the largest area.
     * If more than one Ellipse has the same price, returns the first.
     * If there are no ellipses, returns null
     */
    public Ellipse largest()
    {
        if (list.size() == 0)
        {
            return null;
        }

        Ellipse largest = list.get(0);
        for (Ellipse e : list)
        {
            if ( area(e)  > area(largest))
            {
                largest = e;
            }
        }
        return largest;
    }
    
    /*
     * private helper method. It is not nessecary to use a helper method
     * but it simplifies the code and makes it easier to read
     */
    private double area(Ellipse e)
    {
        return Math.PI * (e.getWidth() / 2) * (e.getHeight() / 2);
    }

 //SHOW   

    @Override
    public String toString()
    {
        return list.toString();
    }
}
