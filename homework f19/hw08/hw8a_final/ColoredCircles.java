//SOLUTION
//SHOW
import java.util.Random;

/**
 * A class representing rows of filled circlew
 */
public class ColoredCircles
{
    public static final int ROWS = 5;
    public static final int COLUMNS = 4;
    public static final int RED = 0;
    public static final int BLUE = 1;
    public static final int BLACK = 2;
    public static final int GREEN = 3;
    public static final int NUMBER_OF_COLORS = 4;

    private int xStart;
    private int yStart;
    private int diameter;
    private Random gen;

    /**
     * Constructs a XRectangle at the given x, y
     * @param theX x coordinate of the upper hand 
     * corner of this Object
     * @param theY y the upper-left hand 
     * corner oft his Object
     */
    public ColoredCircles(int theX, int theY, int diameter)
    {
        gen = new Random(54319576);
        
        //finish the constructor
//HIDE
        xStart = theX;
        yStart = theY;
        this.diameter = diameter;
        
//SHOW        
    }

    /**
     * Draws this ColoredCircle
     */
    public void draw()
    {
        //implement the method 
        
//HIDE                
        int x = xStart;
        int y = yStart;

        for (int row = 0; row < ROWS; row++)
        {
            for (int col = 0; col < COLUMNS; col++)
            {
                Ellipse circle  = new Ellipse(x, y, diameter, diameter);
                circle.setColor(getRandomColor());
                circle.fill();
                x = x + diameter;
            }
            y = y + diameter;
            x = xStart;
        }
//SHOW         
    }
   
    /**
     * Gets a random color 
     * @return a random color Color.RED, Color.GREEN, Color.BLUE 
     * or Color.BLACK
     */
    public Color getRandomColor()
    {
        //implement the method to get a random color using the
        //given Random object. Be sure to use the constants
//HIDE
        int random = gen.nextInt(NUMBER_OF_COLORS);
        Color color;
        if (random == RED)
        {
            color = Color.RED;
        }
        else if (random == BLUE)
        {
            color = Color.BLUE;
        }
        else if (random == BLACK)
        {
            color = Color.BLACK;
        }
        else
        {
            color = Color.GREEN;
        }
        
        return color;
//SHOW
    }
}
