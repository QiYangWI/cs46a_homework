//SOLUTION
import java.util.ArrayList;
public class DogList
{
    public static void main(String[] args)
    {
        ArrayList<String>  dogs = new ArrayList<String>();
        dogs.add("Pit Bull");
        dogs.add("Collie");
        dogs.add("Irish Setter");
        dogs.add("Rottweiler");
        dogs.add("Boxer");
        
        for (String c : dogs)
        {
            System.out.println(c);
        }
    }
}
