//SOLUTION
//SHOW
/**
 * Models a television with a brand and a 
 * screen size in inches 
 */
public class TV 
//HIDE
   implements Comparable
//SHOW
{
     // your code here    
//HIDE


    
//HIDE
    private String brand;
    private double size;
    
    /**
     * Constructs a TV object
     * @param brand the manufacturer
     * @param size the size of this TV
     */
    public TV(String brand, double size)
    {
        this.brand = brand;
        this.size = size;
    }
    
    /**
     * Gets the brand of this TV
     * @return the brand of this TV object
     */
    public String getBrand()
    {
        return brand;
    }
    
    /**
     * Gets the clock speed of this TV
     * @return the size of this TV object
     */
    public double getSize()
    {
        return size;
    }
    
    /**
     * Compares two objects
     * @param other the object to compare
     * @return a positive, negative or 0 integer
     */
    public int compareTo(Object other)
    {
        TV otherTV = (TV)other;
        if (this.equals(otherTV))
        {
            return 0;
        }
        
        if (this.size == otherTV.size)
        {
            return this.brand.compareTo(otherTV.brand);
        }
        else if (this.size < otherTV.size)
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }
    
//SHOW
    /**
     * Gets a string representation of the object
     * @return a string representation of the object
     */
    public String toString()
    {
        String s = getClass().getName()
                + "[brand=" + brand
                + ",size=" + size 
                + "]";
        return s;
    }
}
