//SOLUTION
/**
 * Models a job for papering a room.
 * @author KOBrien
 */
public class WallpaperJob
{
    public static final int SQ_INCHES_PER_SQ_FOOT = 144;
    public static final double WALL_HEIGHT_IN_FEET = 8;
    public static final double DOOR_HEIGHT_IN_INCHES = 80;
    public static final double DOOR_WIDTH_IN_INCHES = 32;
    public static final double COST_PER_ROLL = 40;
    public static final double LABOR_COST_PER_SQ_FOOT = 1.00;
    public static final double WIDTH_OF_WALLPAPER_ROLL_IN_FEET = 33;
    public static final double LENGTH_OF_WALLPAPER_ROLL_INCHES = 27;
    
    private double roomLength;
    private double roomWidth;
    
    /**
     * Constructs a WallpaperJob for a room of the given length and height
     * @param length the length of the room in this WallpaperJob
     * @param width the width of the room in this WallpaperJob
     */
    public WallpaperJob(double length, double width)
    {
        roomLength = length;
        roomWidth = width;
    }
    
    /**
     * Gets the length of this room in this job
     * @return the length of the room in this job
     */
    public double getLength()
    {
        return roomLength;
    }
    
    
    /**
     * Gets the width of this room in this job
     * @return the width of the room in this job
     */
    public double getWidth()
    {
        return roomWidth;
    }
    
    /**
     * Sets a new length and width for the room in this WallpaperJob
     * @param theLength the length of the room in this WallpaperJob
     * @param theWidth the width of the room in this WallpaperJob 
     */
    public void setDimensions(double theLength, double theWidth)
    {
        roomLength = theLength;
        roomWidth = theWidth;
    }
}
