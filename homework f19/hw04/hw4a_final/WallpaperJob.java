//SOLUTION
/**
 * Models a job for papering a room.
 * @author KOBrien
 */
public class WallpaperJob
{
    public static final int SQ_INCHES_PER_SQ_FOOT = 144;
    public static final int INCHES_PER_FOOT = 12;
    public static final double WALL_HEIGHT_IN_FEET = 8;
    public static final double DOOR_HEIGHT_IN_INCHES = 80;
    public static final double DOOR_WIDTH_IN_INCHES = 32;
    public static final double COST_PER_ROLL = 40;
    public static final double LABOR_COST_PER_SQ_FOOT = 1.10;
    public static final double WIDTH_OF_WALLPAPER_ROLL_IN_INCHES = 27;
    public static final double LENGTH_OF_WALLPAPER_ROLL_IN_FEET = 33;
    
    private double roomLength;
    private double roomWidth;
    
    /**
     * Constructs a WallpaperJob for a room of the given length and height
     * @param length the length of the room in this WallpaperJob
     * @param width the width of the room in this WallpaperJob
     */
    public WallpaperJob(double length, double width)
    {
        roomLength = length;
        roomWidth = width;
    }
    
    /**
     * Gets the length of this room in this job
     * @return the length of the room in this job
     */
    public double getLength()
    {
        return roomLength;
    }
    
    
    /**
     * Gets the width of this room in this job
     * @return the width of the room in this job
     */
    public double getWidth()
    {
        return roomWidth;
    }
    
    /**
     * Sets a new length and width for the room in this WallpaperJob
     * @param theLength the length of the room in this WallpaperJob
     * @param theWidth the width of the room in this WallpaperJob 
     */
    public void setDimensions(double theLength, double theWidth)
    {
        roomLength = theLength;
        roomWidth = theWidth;
    }
    
    /**
     * Gets the surface area to wallpaper. 
     * (All four walls (minus the door) plus the ceiling) 
     * @return the surface area to paper
     */
    public double getArea() 
    {
        double wallArea = 2 * WALL_HEIGHT_IN_FEET * roomLength
             + 2 * WALL_HEIGHT_IN_FEET * roomWidth;
        double ceilingArea = roomLength * roomWidth;
        double doorArea = DOOR_HEIGHT_IN_INCHES 
            * DOOR_WIDTH_IN_INCHES 
            / SQ_INCHES_PER_SQ_FOOT; 
        double totalArea = wallArea - doorArea + ceilingArea;
        return totalArea;        
    }
    
    /**
     * Gets the cost of the wallpaper for the job.
     * @return  the cost of the wallpaper for the job.
     */
    public double getCostOfWallpaper()
    {
        double areaOfWallpaperRoll = WIDTH_OF_WALLPAPER_ROLL_IN_INCHES
             / INCHES_PER_FOOT
             * LENGTH_OF_WALLPAPER_ROLL_IN_FEET;
        double numberOfRolls = getArea() / areaOfWallpaperRoll;
        double totalPaperCost = numberOfRolls * COST_PER_ROLL;
        return totalPaperCost;
             
    }
    
    /**
     * Gets the cost of this WallpaperJob.
     * @return  the cost of this WallpaperJob.
     */
    public double getJobCost()
    {
        double costOfLabor = LABOR_COST_PER_SQ_FOOT * getArea();
        return costOfLabor + getCostOfWallpaper();
    }
}
