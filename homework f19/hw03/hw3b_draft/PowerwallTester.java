
/**
 * Write a description of class PowerwallTester here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class PowerwallTester
{
    public static void main(String[] args)
    {
        Powerwall power = new Powerwall(30);
        System.out.println(power.getMaxCapacity());
        System.out.println("Expected: 30");
        System.out.println(power.getCurrentCharge());
        System.out.println("Expected: 0"); //won't be correct until the final
        power.use(10);
        System.out.println(power.getCurrentCharge());
        System.out.println("Expected: 0");
        power.charge(50);
        System.out.println(power.getCurrentCharge());
        System.out.println("Expected: 0");//won't be correct until the final
        
        Powerwall power2 = new Powerwall(20);
        System.out.println(power2.getMaxCapacity());
        System.out.println("Expected: 20");
        System.out.println(power2.getCurrentCharge());
        System.out.println("Expected: 0"); //won't be correct until the final
        
        
    }
}