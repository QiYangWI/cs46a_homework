
/**
 * Tests the RailroodCrossingSignal class.
 *
 * @author kobrien
 * @version (a version number or a date)
 */

public class RailroadCrossingSignalViewer
{
    public static void main(String[] args)
    {
        RailroadCrossingSignal signal = new RailroadCrossingSignal(40, 20);
        signal.draw();
        
        RailroadCrossingSignal signal2 = new RailroadCrossingSignal(50, 100);
        signal2.draw();
    }
}
