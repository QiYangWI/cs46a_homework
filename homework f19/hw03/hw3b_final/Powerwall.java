//SOLUTION
/**
 * Simulars a Powerwall attached to solor panels
 *
 * @author KOBrien
 */
public class Powerwall
{

    private int maxCapacity;
    private int currentCharge;

    /**
     * Constructs a Powerwall with the given capacity
     * @param theMaxCapacity the maximum capcity of this Powerwall
     */
    public Powerwall(int theMaxCapacity)
    {
        maxCapacity = theMaxCapacity;
        currentCharge = theMaxCapacity;
    }
    
    /**
     * Gets the maximum capacity of this Powerwall
     * @return the maximum capacity of this Powerwall
     */
    public int getMaxCapacity()
    {
        return maxCapacity;
    }
    
    /**
     * Gets the current charge of this Powerwall
     * @return the current charge of this Powerwall
     */
    public int getCurrentCharge()
    {
        return currentCharge;
    }
    
    /**
     * Somulates using some of the charge in the Powerwall
     * @param amountToUse the amount to use
     */
    public void use(int amountToUse)
    {
        currentCharge = currentCharge - amountToUse;
    }
    
    /**
     * Simulates adding electricty to the Powerwall
     * @param amountToAdd the amount of electricity to add to the Powerwall
     * 
     */
    public void charge(int amountToAdd)
    {
        currentCharge = currentCharge + amountToAdd;
    }

    
}
