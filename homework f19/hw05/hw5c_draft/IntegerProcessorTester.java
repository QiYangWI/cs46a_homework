
/**
 * Tester for IntegerProcessor
 *
 * @author KOBrien
 */
public class IntegerProcessorTester
{
    public static void main(String[] args)
    {
        IntegerProcessor processor = new IntegerProcessor(500000);
        System.out.println(processor.getInteger());
        System.out.println("Expected: 500000");
        
        processor = new IntegerProcessor(-99);
        System.out.println(processor.getInteger());
        System.out.println("Expected: 99");
        
        processor.setInteger(-100);
        System.out.println(processor.getInteger());
        System.out.println("Expected: 100");
        
        processor.setInteger(1596);
        System.out.println(processor.getInteger());
        System.out.println("Expected: 1596");
        
    }
}
