//SOLUTION

/**
 * Models a AustrailianBeardedDragon climbing a tree
 * 
 * @author KOBrien
 */
public class AustrailianBeardedDragon
{
    public static final int TREE_TOP = 10; 
    private int position;
    private String gender;

    /**
     * Constructs a new AustrailianBeardedDragon with the given gender and position. 
     * @param theGender the gender of this AustrailianBeardedDragon
     * @param thePosition the starting potition on the tree
     */
    public AustrailianBeardedDragon (String theGender, int thePosition)
    {
        setGender(theGender);
        if (thePosition <0)
        {
            position = 0;
        }
        else if (thePosition > TREE_TOP)
        {
            position = TREE_TOP;
        }
        else
        {
            position = thePosition;
        }
    }

    /**
     * Gets the gender of this AustrailianBeardedDragon
     * @return the gender of this AustrailianBeardedDragon
     */
    public String getGender()
    {
        return gender;
    }

    /**
     * Gets the position on the tree of this AustrailianBeardedDragon
     * @return the position on the tree of this AustrailianBeardedDragon
     */
    public int getPosition()
    {
        return position;
    }

    /**
     * Sets the new gender for this AustrailianBeardedDragon
     * @param newGender the new gender for this AustrailianBeardedDragon
     */
    public void setGender(String newGender)
    {
        if (!newGender.equals("male") && !newGender.equals("female"))
        {
            gender = "female";
        }
        else
        {
            gender = newGender;
        }

    }
    /**
     * Climb one unit on the tree
     */
    public void climb()
    {
        if (position + 1 <= TREE_TOP)
        {

            position = position + 1;
        }

    }

    /**
     * returns the AustrailianBeardedDragon to the bottom of the tree.
     */
    public void slide()
    {
        position = 0;
    }  
}
