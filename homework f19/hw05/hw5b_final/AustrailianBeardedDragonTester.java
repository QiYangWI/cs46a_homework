
/**
 * Tests the AustrailianBeardedDragon class
 * 
 * @author Kathleen O'Brien
 *
 */
public class AustrailianBeardedDragonTester
{
    public static void main(String[] arg)
    {
        //regression test
        AustrailianBeardedDragon dragon1 = new AustrailianBeardedDragon("male", 4);
        System.out.println(dragon1.getGender());
        System.out.println("Expected: male");
        System.out.println(dragon1.getPosition());
        System.out.println("Expected: 4");
        
        //test bad postions in constructor
        dragon1 = new AustrailianBeardedDragon("male", -4);
        System.out.println(dragon1.getPosition());
        System.out.println("Expected: 0");
        dragon1 = new AustrailianBeardedDragon("male", 11);
        System.out.println(dragon1.getPosition());
        System.out.println("Expected: 10");
        
        //test climb beyond top
        AustrailianBeardedDragon dragon2 = new AustrailianBeardedDragon("female", 9);
        dragon2.climb();
        System.out.println(dragon2.getPosition());
        System.out.println("Expected: 10"); 
        dragon2.climb();
        System.out.println(dragon2.getPosition());
        System.out.println("Expected: 10"); 

        System.out.println(dragon2.getGender());
        System.out.println("Expected: female");
        
        //test bad gender in constructor
        AustrailianBeardedDragon dragon3 = new AustrailianBeardedDragon("mal", 9);
        System.out.println(dragon3.getGender());
        System.out.println("Expected: female");
        
        //test bad setGender
        dragon3.setGender("male");
        System.out.println(dragon3.getGender());
        System.out.println("Expected: male");
        dragon3.setGender("alligator");
        System.out.println(dragon3.getGender());
        System.out.println("Expected: female");
           
        
    }
 
}
