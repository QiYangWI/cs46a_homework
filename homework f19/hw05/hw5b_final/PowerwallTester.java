
/**
 * Test error checking in  Powerwall class.
 *
 * @author (KOBrien
 * @version (a version number or a date)
 */
public class PowerwallTester
{
    public static void main(String[] args)
    {
        
        //Test negative max caacity
        Powerwall power = new Powerwall(-10);
        System.out.println(power.getMaxCapacity());
        System.out.println("Expected: 50");
        System.out.println(power.getCurrentCharge());
        System.out.println("Expected: 50");
        
        //do regression test. Does valid max capacity still work?
        power = new Powerwall(30);
        System.out.println(power.getMaxCapacity());
        System.out.println("Expected: 30");
        System.out.println(power.getCurrentCharge());
        System.out.println("Expected: 30"); 
        
        //use negative
        power.use(-5);
        System.out.println(power.getCurrentCharge());
        System.out.println("Expected: 30");
        
        //use too much
        power.use(35);
        System.out.println(power.getCurrentCharge());
        System.out.println("Expected: 0");
        
        //do regression test. Does valid charge still work?
        power.charge(20);
        System.out.println(power.getCurrentCharge());
        System.out.println("Expected: 20");
        
        //charge too much
        power.charge(50);
        System.out.println(power.getCurrentCharge());
        System.out.println("Expected: 30");
        
        //do regression test. Does valid usage still work?
        power.use(20);
        System.out.println(power.getCurrentCharge());
        System.out.println("Expected: 10");
        
        //charge negative
        power.charge(-7);
        System.out.println(power.getCurrentCharge());
        System.out.println("Expected: 10");
        
    }
}