
/**
 * Tests HomeLoanProcessor class.
 *
 * @author KOBrien
 * 
 */
public class HomeLoanProcessorTester
{
    public static void main(String[] args)
    {
        //test more than $500000 loan amount
        HomeLoanProcessor loan = new HomeLoanProcessor(500001.0, 125000.25, 3 );
        System.out.println("Granted: " + loan.loanGranted());
        System.out.println("Expected: true");
        loan = new HomeLoanProcessor(600000.0, 125000, 3 );
        System.out.println("Granted: " + loan.loanGranted());
        System.out.println("Expected: false");
        
        loan = new HomeLoanProcessor(600001.0, 250000, 2 );
        System.out.println("Granted: " + loan.loanGranted());
        System.out.println("Expected: true");
        loan = new HomeLoanProcessor(600001.0, 200000, 2 );
        System.out.println("Granted: " + loan.loanGranted());
        System.out.println("Expected: false");
        loan = new HomeLoanProcessor(600001.0, 300000, 1 );
        System.out.println("Granted: " + loan.loanGranted());
        System.out.println("Expected: false");
        
        //test more than $200000 loan amount
        loan = new HomeLoanProcessor(200000.01, 40000.002 , 7);
        System.out.println("Granted: " + loan.loanGranted());
        System.out.println("Expected: true");
        loan = new HomeLoanProcessor(200000.01, 40000.002 , 6);
        System.out.println("Granted: " + loan.loanGranted());
        System.out.println("Expected: false");
        loan = new HomeLoanProcessor(200000.01, 40000 , 7);
        System.out.println("Granted: " + loan.loanGranted());
        System.out.println("Expected: false");
        
        loan = new HomeLoanProcessor(200000.01, 50000.0025 , 4);
        System.out.println("Granted: " + loan.loanGranted());
        System.out.println("Expected: true");
        loan = new HomeLoanProcessor(200000.01, 50000.0025 , 3);
        System.out.println("Granted: " + loan.loanGranted());
        System.out.println("Expected: false");
        loan = new HomeLoanProcessor(200000.01, 50000.00 , 4);
        System.out.println("Granted: " + loan.loanGranted());
        System.out.println("Expected: false");
        
        loan = new HomeLoanProcessor(200000.01, 66666.64 , 0);
        System.out.println("Granted: " + loan.loanGranted());
        System.out.println("Expected: true");
        loan = new HomeLoanProcessor(199999.99, 66666.63 , 20);
        System.out.println("Granted: " + loan.loanGranted());
        System.out.println("Expected: false");
        
        //test setting invalid loan amount
        loan.setLoanAmount(0.0);                
        System.out.println("Loan: " + loan.getLoanAmount());
        System.out.println("Expected: 0.0");
        System.out.println("Income: " + loan.getAnnualIncome());
        System.out.println("Expected: 0.0");
        System.out.println("Years: " + loan.getYearsAtCurrentAddress());
        System.out.println("Expected: 0");
                
        //test seeting valid loan amount
        loan = new HomeLoanProcessor(500001.0, 125000.25, 3 );
        loan.setLoanAmount(500002.0);
        System.out.println("Granted: " + loan.loanGranted());
        System.out.println("Expected: false");
       
    }
}
