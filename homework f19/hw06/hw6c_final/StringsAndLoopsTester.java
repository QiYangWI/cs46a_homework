
/**
 * Tester for StringsAndLoops.
 *
 * @author KOBrien
 * @version (a version number or a date)
 */
public class StringsAndLoopsTester
{
    public static void main(String[] arga)
    {
        StringsAndLoops phrase = new StringsAndLoops("Today IS A good DAy tO Code!");
        System.out.println(phrase.getACount());
        System.out.println("Expected: 3");
        System.out.println(phrase.firsts());
        System.out.println("Expected: TIAgDtC");
        System.out.println(phrase.getText());
        System.out.println("Expected: Today IS A good DAy tO Code!");        
        
        phrase = new StringsAndLoops("");
        System.out.println(phrase.getACount());
        System.out.println("Expected: 0");
        System.out.println(phrase.firsts());
        System.out.println("Expected: ");
        
        phrase = new StringsAndLoops("aAbB is bBaA");
        System.out.println(phrase.getACount());
        System.out.println("Expected: 4");
        System.out.println(phrase.firsts());
        System.out.println("Expected: aib");
        System.out.println(phrase.getText());
        System.out.println("Expected: aAbB is bBaA");
        
    }
}
