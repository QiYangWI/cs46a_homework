//SOLUTION
//SHOW
/**
 * Processes an opplication for a home loan
 */
public class HomeLoanProcessor
{
    private double loanAmount;
    private double annualIncome;
    private int yearsAtCurrentAddress;
    
    //Add your code

//HIDE    
    /**
     * Constructor for objects of class HomeLoanProcessor with the 
     * given parameters
     * @param loanAmount the amount of loan requested
     * @param annualIncome the annual income of those requestion the loan
     * @param yearsAtCurrentAddress  the number of years the applicant has lived
     * at the curreent address
     */
    public HomeLoanProcessor(double loanAmount, double annualIncome, int yearsAtCurrentAddress)
    {
        if ((loanAmount <= 0) || (annualIncome <= 0) || (yearsAtCurrentAddress <= 0))
        {
            this.loanAmount = 0;
            this.annualIncome = 0;
            this.yearsAtCurrentAddress = 0;
        }
        else
        {
            this.loanAmount = loanAmount;
            this.annualIncome = annualIncome;
            this.yearsAtCurrentAddress = yearsAtCurrentAddress;
        }
    }
    
    /**
     * Gets the annual income
     * @return the annual income.
     */
    public double getAnnualIncome()
    {
        return annualIncome;
    }
    
    /**
     * Sets a new annual income. If less than or equal to 0, 
     * if <= 0, set loan amount, annual income, and years at current address to 0.
     * @param income the new annual income
     */
    public void setAnnualIncome( double income)
    {
        if (income <= 0)
        {
            this.loanAmount = 0;
            this.annualIncome = 0;
            this.yearsAtCurrentAddress = 0;
        }
        else
        {
            this.annualIncome = annualIncome;
        }
    }
    
    /**
     * Sets a new loan amount
     * @param amount the new loan amount to set.
     * if <= 0, set loan amount, annual income, and years at current address to 0.
     */
    
    public void setLoanAmount(double amount)
    {
       
    }
    
    /**
     * Determines if this loan is granted
     * @return true if application meets the stated requirements. 
     * Otherwise false
     */
    public boolean loanGranted()
    {
        return false;
    }
    
//SHOW
    /**
     * Gets the loan amount
     * @return the amount of this loan
     */
    public double getLoanAmount()
    {
        return loanAmount;
    }
    
    /**
     * gets the number of years at the current address
     * @return the number of years at the current address
     */
    public int getYearsAtCurrentAddress()
    {
        return yearsAtCurrentAddress;
    }
    
}
