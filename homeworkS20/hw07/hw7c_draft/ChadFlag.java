//SOLUTION
public class ChadFlag
{
    private Picture pic;
    private int width;
    private int height;


    public Color getColorAt(int x, int y)
    {
        Color c = Color.WHITE;
        if ((x < width / 3) )
        {
            c = new Color(00, 00, 205);
        }

        
        else
        {
            c = Color.BLACK;
        }

        return c; 
    }

    public ChadFlag(int width, int height)
    {
        this.width = width;
        this.height = height;
        pic = new Picture(width, height);
        pic.draw();
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Color c = getColorAt(x, y);
                pic.setColorAt(x, y, c);
            }
        }
    }
}
