//SOLUTION
//SHOW
import java.util.ArrayList;
/**
 * Manages a cllection of Ellipse objects
 *
 */
public class EllipseManager
{
    //add your constructor and methods
    
//HIDE
    private ArrayList<Ellipse> list;

    /**
     * Constructs an empty EllipseManager
     */
    public EllipseManager()
    {
        list = new ArrayList<Ellipse>();
    }

    /**
     * Adds the Ellipse to this EllipseManager
     * @param be the Ellipse to add
     */
    public void add(Ellipse e)
    {
        list.add(e);
    }

 //SHOW   

    @Override
    public String toString()
    {
        return list.toString();
    }
}
