
/**
 * Tests the EllipseManager class.
 *
 * @author Kathleen O'Brien
 */
public class EllipseManagerTester
{
    public static void main(String[] args)
    {
        EllipseManager manager = new EllipseManager();
        manager.add(new Ellipse(0, 0, 45, 90)); 
        manager.add(new Ellipse(0, 0, 100, 50)); 
        manager.add(new Ellipse(0, 0, 200, 100)); 
        manager.add(new Ellipse(0, 0, 50, 40));  
        System.out.println(manager);
        System.out.println("Expected: [Ellipse[x=0,y=0,width=45,height=90], Ellipse[x=0,y=0,width=100,height=50], Ellipse[x=0,y=0,width=200,height=100], Ellipse[x=0,y=0,width=50,height=40]]");
    }
}
