//SOLUTION
import java.util.ArrayList;
public class DogList
{
    public static void main(String[] args)
    {
        ArrayList<String>  dogs = new ArrayList<String>();
        dogs.add("Pit Bull");
        dogs.add("Collie");
        dogs.add("rish Setter");
        dogs.add("Rottweiler");
        dogs.add("Boxer");
        dogs.add(2, "Siberian Husky");
        dogs.set(3, "Great Dane");
        dogs.set(dogs.size() - 2, "Malamute");
        dogs.remove("Pit Bull");

        System.out.println(dogs.get(2) + "***");
        System.out.println(dogs.toString());

        for (String c : dogs)
        {
            System.out.println(c);
        }
    }
}
