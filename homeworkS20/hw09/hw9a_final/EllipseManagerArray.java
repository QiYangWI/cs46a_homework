//SOLUTION
//SHOW
import java.util.Arrays;
/**
 * Manages a collection of Ellipse objects
 *
 */
public class EllipseManagerArray
{
    //add your constructor and methods
    
//HIDE
    private Ellipse[] ellipses;

    /**
     * Constructs EllipseManagerArray with the given array
     * @param array the array for this EllipseManagerArray;
     */
    public EllipseManagerArray(Ellipse[] array)
    {
        ellipses = array;
    }

    /**
     * Gets the average area of the Ellipse in this EllipseManagerArray
     * @return the average area of the Ellipse in this EllipseManagerArray
     */
    public double average()
    {
        double sum = 0;
        for (Ellipse e: ellipses)
        {
            sum = sum + area(e);
        }
        
        double average = 0;
        if (ellipses.length > 0)
        {
            average = sum / ellipses.length;
        } 
        
        return average;
    }
    
     /**
     * Swaps the elements at the given indices if they are both in bounds
     * @param index1 the first index to swap
     * @param index2 the second index
     */
    public void exchange(int index1, int index2)
    {
        if (index1 >= 0 && index2 >= 0 
        && index1 < (ellipses.length) 
        && index2 < (ellipses.length))
        {
            Ellipse temp = ellipses[index1];
            ellipses[index1] = ellipses[index2];
            ellipses[index2] = temp;       
        }
    }
    
     /**
     * Gets the Ellipse with the largest are. 
     * @return the Ellipse with the largest area.
     * If more than one Ellipse has the same area, returns the first.
     * If there are no ellipses, returns null
     */
    public Ellipse largest()
    {
        if (ellipses.length == 0)
        {
            return null;
        }

        Ellipse largest = ellipses[0];
        for (Ellipse e : ellipses)
        {
            if ( area(e)  > area(largest))
            {
                largest = e;
            }
        }
        return largest;
    }
    
    /*
     * private helper method. It is not nessecary to use a helper method
     * but it simplifies the code and makes it easier to read
     */
    private double area(Ellipse e)
    {
        return Math.PI * (e.getWidth() / 2) * (e.getHeight() / 2);
    }

 //SHOW   

    @Override
    public String toString()
    {
        return Arrays.toString(ellipses);
    }
}
