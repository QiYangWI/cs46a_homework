import java.util.Arrays;
public class IntArrayTester
{
    public static void main(String[] args)
    {
        int[] list = {5, 3, 7, -2, 4, 7, 6};  
        IntArray util = new IntArray(list);
        
        int[] list2 = {-7, -2, -9, -4, -7};  
        IntArray util2 = new IntArray(list2);
        
        int[] list3 = new int[0];  
        IntArray util3 = new IntArray(list3);
        
        System.out.println(util.negatives());
        System.out.println("Expected:  -2");
        
        System.out.println(util2.negatives());
        System.out.println("Expected:  -7, -2, -9, -4, -7");
        
        System.out.println(util3.negatives());
        System.out.println("Expected:  ");
        
        System.out.println(util.negativeCount());
        System.out.println("Expected:  1");
        System.out.println(util2.negativeCount());
        System.out.println("Expected:  5");
        System.out.println(util3.negativeCount());
        System.out.println("Expected:  0");
        
    }
}
