//SOLUTION
/**
 * Models a tic tac toe game board
 *
 * @author KOBrien
 */
public class TicTacToeGame
{
    public static final int NONE = 0; //indicates a free space
    public static final int PLAYER_1 = 1; //indicates a space marked by player 1
    public static final int PLAYER_2 = 2; //indicates a space marked by player 2
    public static final int SIZE = 3;
    private int[][] game;
    
    /**
     * Constructs a TicTacToeGame represented by the 2-d array
     * @param currentBoard a representaion of the state of the game
     * 
     */
    public TicTacToeGame(int[][] currentBoard)
    {
        game = currentBoard;
    }
    
    /**
     * Gets a string representation of the game board
     * @return a string representation of the game board
     */
    public String getBoard()
    {
        String board = "";
        for (int row = 0; row < SIZE; row++)
        {
            for (int column = 0; column < SIZE; column++)
            {
                board = board + game[row][column] + " ";
            }
            board = board + "\n";
        }
        return board;
    }
    
    /**
     * Gets the winner of the game
     * @return an integer representing the winner or -1 if a row, 
     * column or diagonal contains three 0's
     */
    public int winner()
    {
        final int NO_WINNER_FOUND = 100; //a flag to say there were no matches
        int winner = NO_WINNER_FOUND; 
        if (rowIsWinner(0))
            winner = game[0][0];
        else if (rowIsWinner(1))
            winner = game[1][0];
        else if (rowIsWinner(2))
            winner = game[2][0];
            
        else if (columnIsWinner(0))
        {
            winner = game[0][0];
        }
        else if (columnIsWinner(1))
        {
            winner = game[0][1];
        }
        else if (columnIsWinner(2))
        {
            winner = game[0][2];
        }
        
        else if (diagonalIsWinner())
        {
            winner = game[1][1];   
        }
        
        //no three should be NONE
        if (winner == NONE)
        {
            winner = -1; //indicate error
        }
        else if (winner == NO_WINNER_FOUND)
        {
            winner = NONE;
        }
        
        return winner;
    }
    
    /**
     * Gets an indicating of the number of moves by each player
     * @return an int array where array[0] is the number of unused spaces, array[1] is the number of moves made by player 1, and array[2] is the number of moves made by player 2.
     */
    public int[] counts()
    {
        int[] counts = new int[3];
        for (int row = 0; row < SIZE; row++)
        {
            for (int column = 0; column < SIZE; column++)
            {
                int player = game[row][column];
                if (player == NONE)
                {
                    counts[0]++;
                }
                else if (player == PLAYER_1)
                {
                    counts[1]++;
                }
                else 
                {
                    counts[2]++;
                }
                
            }
        }
        return  counts;
    }
    
    // returns true if the three values in the given row are the same
    
    private boolean rowIsWinner(int row)
    {
        return (game[row][0] == game[row][1]) 
               && (game[row][1] == game[row][2]);
    }
    
    // returns true if the three values in the given column are the same
    private boolean columnIsWinner(int column)
    {
        return game[0][column] == game[1][column]
               && game[1][column] == game[2][column];
    }
    
    //returns true if the three values in the either diagonal are the same
    private boolean diagonalIsWinner()
    {
        boolean diagonal1Winner = game[0][0] == game[1][1]
               && game[1][1] == game[2][2];
        boolean diagonal2Winner = game[0][2] == game[1][1]
               && game[1][1] == game[2][0];
               
        return diagonal1Winner || diagonal2Winner;
    }
}
