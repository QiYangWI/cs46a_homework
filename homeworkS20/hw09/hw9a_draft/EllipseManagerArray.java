//SOLUTION
//SHOW
import java.util.Arrays;
/**
 * Manages a collection of Ellipse objects
 *
 */
public class EllipseManagerArray
{
    //add your constructor and methods
    
//HIDE
    private Ellipse[] ellipses;

    /**
     * Constructs EllipseManagerArray with the given array
     * @param array the array for this EllipseManagerArray;
     */
    public EllipseManagerArray(Ellipse[] array)
    {
        ellipses = array;
    }

    /**
     * Gets the average area of the Ellipse in this EllipseManagerArray
     * @return the average area of the Ellipse in this EllipseManagerArray
     */
    public double average()
    {
        double sum = 0;
        for (Ellipse e: ellipses)
        {
            sum = sum + area(e);
        }
        
        double average = 0;
        if (ellipses.length > 0)
        {
            average = sum / ellipses.length;
        } 
        
        return average;
    }
    
    
    /*
     * private helper method. It is not nessecary to use a helper method
     * but it simplifies the code and makes it easier to read
     */
    private double area(Ellipse e)
    {        
        return Math.PI * e.getWidth() / 2.0 * e.getHeight() / 2.0;
    }

 //SHOW   

    @Override
    public String toString()
    {
        return Arrays.toString(ellipses);
    }
}
