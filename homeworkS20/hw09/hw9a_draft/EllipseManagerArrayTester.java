
/**
 * Tests the EllipseManager class.
 *
 * @author Kathleen O'Brien
 */
public class EllipseManagerArrayTester
{
    public static void main(String[] args)
    {
        Ellipse[] array = {
            new Ellipse(0, 0, 45, 90), 
            new Ellipse(0, 0, 100, 50),
            new Ellipse(0, 0, 200, 100), 
            new Ellipse(0, 0, 50, 40)  
            };
        EllipseManagerArray manager = new EllipseManagerArray(array);        
        System.out.printf("Average: %.4f%n", manager.average());
        System.out.println("Expected: 6096.6532");
        
        Ellipse[] array2 = {
            new Ellipse(0, 0, 45, 90), 
            new Ellipse(0, 0, 100, 50),
            new Ellipse(0, 0, 200, 100), 
            new Ellipse(0, 0, 50, 40),
            new Ellipse(0, 0, 100, 200)
            };
        EllipseManagerArray manager2 = new EllipseManagerArray(array2); 
        System.out.printf("Average: %.4f%n", manager2.average());
        System.out.println("Expected: 8018.9152");
        
        //test with empty object
        array = new Ellipse[0];
        manager = new EllipseManagerArray(array);
        
        System.out.println(manager.average());
        System.out.println("Expected: 0.0");
            
    }
}
