//SOLUTION
/**
 * Models a tic tac toe game board
 *
 * @author KOBrien
 */
public class TicTacToeGame
{
    public static final int NONE = 0; //indicates a free space
    public static final int PLAYER_1 = 1; //indicates a space marked by player 1
    public static final int PLAYER_2 = 2; //indicates a space marked by player 2
    public static final int SIZE = 3;
    private int[][] game;
    
    /**
     * Constructs a TicTacToeGame represented by the 2-d array
     * @param currentBoard a representaion of the state of the game
     * 
     */
    public TicTacToeGame(int[][] currentBoard)
    {
        game = currentBoard;
    }
    
    /**
     * Gets a string representation of the game board
     * @return a string representation of the game board
     */
    public String getBoard()
    {
        return null;
    }
    
    /**
     * Gets the winner of the game
     * @return an integer representing the winner or -1 if a row, 
     * column or diagonal contains three 0's
     */
    public int winner()
    {
        return 0;
    }
    
    /**
     * Gets an indicating of the number of moves by each player
     * @return an int array where array[0] is the number of unused spaces, array[1] is the number of moves made by player 1, and array[2] is the number of moves made by player 2.
     */
    public int[] counts()
    {
        int[] array = {0, 0, 0};
        return  array;
    }
}
