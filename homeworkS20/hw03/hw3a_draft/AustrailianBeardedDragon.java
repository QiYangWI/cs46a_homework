//SOLUTION
//SHOW
/**
 * Models a AustrailianBeardedDragon climbing a tree
 * 
 * @author KOBrien
 */
public class AustrailianBeardedDragon
{
    //HIDE
    private int position;
    private String gender;
    //SHOW
    
    /**
     * Constructs a new AustrailianBeardedDragon with the given gender and position. 
     * @param theGender the gender of this AustrailianBeardedDragon
     * @param thePosition the starting potition on the tree
     */
    public AustrailianBeardedDragon (String theGender, int thePosition)
    {
        //add your code here
        //HIDE
        gender = theGender;
        position = thePosition;
        //SHOW
    }
    
    /**
     * Gets the gender of this AustrailianBeardedDragon
     * @return the gender of this AustrailianBeardedDragon
     */
    public String getGender()
    {
        //add your code here
        //HIDE
        return gender;
        //SHOW
    }
    
    /**
     * Gets the position on the tree of this AustrailianBeardedDragon
     * @return the position on the tree of this AustrailianBeardedDragon
     */
    public int getPosition()
    {
        //add your code here
        //HIDE
        return 0;
        //SHOW
    }
    
    /**
     * Sets the new gender for this AustrailianBeardedDragon
     * @param newGender the new gender for this AustrailianBeardedDragon
     */
    public void setGender(String newGender)
    {
        
        //HIDE
        
        //SHOW
    }
    
    /**
     * Climb one unit on the tree
     */
    public void climb()
    {
        
        //HIDE
        
        //SHOW
    }

    /**
     * returns the AustrailianBeardedDragon to the bottom of the tree.
     */
    public void slide()
    {
        
        //HIDE
        
        //SHOW
    }  
}
