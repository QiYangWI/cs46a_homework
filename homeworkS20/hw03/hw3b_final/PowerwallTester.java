
/**
 * Write a description of class PowerwallTester here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class PowerwallTester
{
    public static void main(String[] args)
    {
        Powerwall power = new Powerwall(30);
        System.out.println(power.getMaxCapacity());
        System.out.println("Expected: 30");
        System.out.println(power.getCurrentCharge());
        System.out.println("Expected: 30"); 
        power.use(10);
        System.out.println(power.getCurrentCharge());
        System.out.println("Expected: 20");
        power.charge(50);
        System.out.println(power.getCurrentCharge());
        System.out.println("Expected: 70");//notice it is filled to over capacity
        
        Powerwall power2 = new Powerwall(20);
        System.out.println(power2.getMaxCapacity());
        System.out.println("Expected: 20");
        System.out.println(power2.getCurrentCharge());
        System.out.println("Expected: 20"); 
        power2.use(25);
        System.out.println(power2.getCurrentCharge());
        System.out.println("Expected: -5"); //notice incorrect value
        power2.charge(15);
        System.out.println(power2.getCurrentCharge());
        System.out.println("Expected: 10");
        power2.use(4);
        System.out.println(power2.getCurrentCharge());
        System.out.println("Expected: 6");
        power2.use(2);
        System.out.println(power2.getCurrentCharge());
        System.out.println("Expected: 4");
        
        
        
    }
}