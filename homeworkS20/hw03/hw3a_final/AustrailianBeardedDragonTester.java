
/**
 * Tests the AustrailianBeardedDragon class
 * 
 * @author Kathleen O'Brien
 *
 */
public class AustrailianBeardedDragonTester
{
    public static void main(String[] arg)
    {
        AustrailianBeardedDragon dragon1 = new AustrailianBeardedDragon("male", 4);
        System.out.println(dragon1.getGender());
        System.out.println("Expected: male");
        System.out.println(dragon1.getPosition());
        System.out.println("Expected: 4");
        dragon1.climb();
        dragon1.climb();
        dragon1.climb();
        System.out.println(dragon1.getPosition());
        System.out.println("Expected: 7");
        dragon1.slide();
        System.out.println(dragon1.getPosition());
        System.out.println("Expected: 0");
        dragon1.setGender("female");
        System.out.println(dragon1.getGender()); 
        System.out.println("Expected: female");
        
        AustrailianBeardedDragon dragon2 = new AustrailianBeardedDragon("female", 9);
        dragon2.climb();
        dragon2.climb();
        dragon2.climb();
        dragon2.climb();
        System.out.println(dragon2.getGender());
        System.out.println("Expected: female");
        System.out.println(dragon2.getPosition());
        System.out.println("Expected: 13"); //note the position is beyond the top of the tree
        dragon2.setGender("female");
        System.out.println(dragon2.getGender()); 
        System.out.println("Expected: female");
        dragon2.setGender("male");
        System.out.println(dragon2.getGender()); 
        System.out.println("Expected: male");       
        
    }
 
}
