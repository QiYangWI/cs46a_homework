//SOLUTION
/**
 * Logo of a railroad crossing signal
 * @author kobrien
 */
public class RailroadCrossingSignal
{
    private int x;
    private int y;

    /**
     * Constructs an object of class RailroadCrossingSignal at the given coordinates
     * @param theX the x coordinate of the upper left hand corner 
     * of the bounding rectangle for the RailroadCrossingSignal
     * @param theY the y coordinate of the upper left hand corner 
     * of the bounding rectangle for the RailroadCrossingSignal
     */
    public RailroadCrossingSignal(int theX, int theY)
    {
        x = theX;
        y = theY;
    }

    /**
     * Draws the Circles at its coordinates
     */
    public void draw()
    {
        Ellipse leftCircle = new Ellipse(x, y, 40, 40);
        leftCircle.setColor(Color.RED);
        leftCircle.fill();


    }
}
