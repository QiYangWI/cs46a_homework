//SOLUTION
import java.util.Scanner;
/**
 * Does some stuff with strings
 *
 * @author KOBrien
 * 
 */
public class Holidays
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("What is your favorite holiday? ");
        String holiday = scan.nextLine();
        System.out.println(holiday);
        String lower = holiday.toLowerCase();
        System.out.println(lower);
        if (lower.equals("thanksgiving"))
        {
            System.out.println("It is good to be thankful");
        }
        else if (lower.equals("new year's day"))
        {
            System.out.println("The start of a new year");
        }
        else if (lower.equals("president's day"))
        {
            System.out.println("Any excuse for a vacation is good");
        }   
        else
        {
            System.out.println("Are you sure that is a holiday?");
        }
        System.out.println(holiday);  
        
    }
}
