//SOLUTION
import java.util.Scanner;
/**
 * Does some stuff with strings
 *
 * @author KOBrien
 * 
 */
public class Holidays
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("What is your favorite holiday? ");
        String holiday = scan.nextLine();
        System.out.println(holiday);
        
    }
}
