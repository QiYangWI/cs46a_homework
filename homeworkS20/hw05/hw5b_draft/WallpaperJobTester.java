
/**
 * Tests the WallpaperJob class.
 *
 * @author KOBrien
 * 
 */
public class WallpaperJobTester
{
    public static void main(String[] args)
    {
        //be sure positive values still work
        WallpaperJob job = new WallpaperJob(10.5, 9.0);
        System.out.println(job.getLength() + " X " + job.getWidth());
        System.out.println("Expected: 10.5 X 9.0");

        job = new WallpaperJob(-10.5, 9.0);
        System.out.println(job.getLength() + " X " + job.getWidth());
        System.out.println("Expected: 0.0 X 9.0");
        job = new WallpaperJob(10.5, -9.0);
        System.out.println(job.getLength() + " X " + job.getWidth());
        System.out.println("Expected: 10.5 X 0.0");
        job = new WallpaperJob(-10.5, -9.0);
        System.out.println(job.getLength() + " X " + job.getWidth());
        System.out.println("Expected: 0.0 X 0.0");

        //be sure positive values still work
        job.setDimensions(15, 12);
        System.out.println(job.getLength() + " X " + job.getWidth());
        System.out.println("Expected: 15.0 X 12.0");

        job.setDimensions(10, -9);
        System.out.println(job.getLength() + " X " + job.getWidth());
        System.out.println("Expected: 10.0 X 0.0");
    }
}
