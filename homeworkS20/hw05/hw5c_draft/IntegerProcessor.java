//SOLUTION
//SHOW
/**
 * Models an integer
 */
public class IntegerProcessor
{
    private int integer;

    //HIDE
    /**
     * Constructor for objects of class IntegerProcessor
     * @param integer the value for this IntegerProcessor
     */
    public IntegerProcessor(int integer)
    {
        setInteger(integer);
    }
    
    /**
     *  Sets a new integer. if value is negative, multiply if by -1 
     *  so that the value is  positive integer. 
     *  @param value the int to set
     */
    public void setInteger(int value)
    {
        if (value >= 0)
        {
            this.integer = value;
        }
        else
        {
            this.integer = value * -1;
        }
    }

    //SHOW
    /**
     * Gets the integer for this IntegerProcessor
     * @return the integer for this IntegerProcessor
     */
    public int getInteger()
    {
        return integer;
    }

}
