//SOLUTION
/**
 * Simulars a Powerwall attached to solor panels
 *
 * @author KOBrien
 */
public class Powerwall
{
    public static final int DEFAULT_MAX_CAPACITY = 50;
    private int maxCapacity;
    private int currentCharge;

    /**
     * Constructs a Powerwall with the given capacity
     * @param theMaxCapacity the maximum capcity of this Powerwall
     */
    public Powerwall(int theMaxCapacity)
    {
        if (theMaxCapacity < 0)
        {
            maxCapacity = DEFAULT_MAX_CAPACITY;
        }
        else
        {
            maxCapacity = theMaxCapacity;
        }
        currentCharge = maxCapacity;
    }

    /**
     * Gets the maximum capacity of this Powerwall
     * @return the maximum capacity of this Powerwall
     */
    public int getMaxCapacity()
    {
        return maxCapacity;
    }

    /**
     * Gets the current charge of this Powerwall
     * @return the current charge of this Powerwall
     */
    public int getCurrentCharge()
    {
        return currentCharge;
    }

    /**
     * Somulates using some of the charge in the Powerwall
     * @param amountToUse the amount to use
     */
    public void use(int amountToUse)
    {
        if (amountToUse > 0)
        {
            currentCharge = currentCharge - amountToUse;
            if (currentCharge < 0)
            {
                currentCharge = 0;
            }
        }

    }

    /**
     * Simulates adding electricty to the Powerwall
     * @param amountToAdd the amount of electricity to add to the Powerwall
     * 
     */
    public void charge(int amountToAdd)
    {
        if (amountToAdd > 0)
        {
            currentCharge = currentCharge + amountToAdd;
            if (currentCharge > maxCapacity)
            {
                currentCharge = maxCapacity;
            }
        }
    }

}
