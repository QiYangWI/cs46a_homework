//SOLUTION
//SHOW
/**
 * Models an integer
 */
public class IntegerProcessor
{
    private int integer;

    //HIDE
    /**
     * Constructor for objects of class IntegerProcessor
     * @param integer the value for this IntegerProcessor
     */
    public IntegerProcessor(int integer)
    {
        setInteger(integer);
    }
    
    /**
     *  Sets a new integer. if value is negative, multiply if by -1 
     *  so that the value is  positive integer. 
     *  @param value the int to set
     */
    public void setInteger(int value)
    {
        if (value >= 0)
        {
            this.integer = value;
        }
        else
        {
            this.integer = value * -1;
        }
    }
    
    /**
     * Determines if the number qualifies as "big" 
     * A big integer is bigger than 500,000,000
     * @return true if the integer is big, otherwise false.
     */
    public boolean isBig()
    {
        final int BIG = 500000;
        if (integer > BIG)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
     /**
     * Determines if the number qualifies as "small" 
     * A small integer is less than 100
     * @return true if the integer is small, otherwise false.
     */
    public boolean isSmall()
    {
        final int SMALL_CEILING = 100;
        return integer < SMALL_CEILING;
    }
    
    /**
     * Gets a string represntation of the number in comma format
     * @return the number in comma format
     */
    
    public String formatWithCommas()
    {
         String commaString = "";
        
        if (integer < 1000)
        {
            commaString = "" + integer;    
        }
        else if (integer < 10000)
        {
            String temp = "" + integer;
            commaString = temp.charAt(0) +"," + temp.substring(1);
        }
        else if (integer < 100000)
        {
            String temp = "" + integer;
            commaString = temp.substring(0, 2) +"," 
               + temp.substring(2);
        }
          else if (integer < 1000000)
        {
            String temp = "" + integer;
            commaString = temp.substring(0, 3) +"," 
               + temp.substring(3);
        }
        else
        {
            commaString = "too big";
        }        
        return commaString;
    }
    
    //SHOW
    /**
     * Gets the integer for this IntegerProcessor
     * @return the integer for this IntegerProcessor
     */
    public int getInteger()
    {
        return integer;
    }

}
