
/**
 * Tester for IntegerProcessor
 *
 * @author KOBrien
 */
public class IntegerProcessorTester
{
    public static void main(String[] args)
    {
        IntegerProcessor processor = new IntegerProcessor(500000);
        System.out.println(processor.isBig());
        System.out.println("Expected: false");
        
        processor = new IntegerProcessor(-500001);
        System.out.println(processor.isBig());
        System.out.println("Expected: true");
        
        processor.setInteger(-100);
        System.out.println(processor.isSmall());
        System.out.println("Expected: false");
        
        processor.setInteger(99);
        System.out.println(processor.isSmall());
        System.out.println("Expected: true");
        
        processor = new IntegerProcessor(1000000);
        System.out.println(processor.formatWithCommas());
        System.out.println("Expected: too big");
        
        processor = new IntegerProcessor(999);
        System.out.println(processor.formatWithCommas());
        System.out.println("Expected: 999");
        
        
        processor = new IntegerProcessor (1000);
        System.out.println(processor.formatWithCommas());
        System.out.println("Expected: 1,000");

        
        processor.setInteger(9999);
        System.out.println(processor.formatWithCommas());
        System.out.println("Expected: 9,999");
        
        processor.setInteger(99999);
        System.out.println(processor.formatWithCommas());
        System.out.println("Expected: 99,999");
        
        processor.setInteger(999999);
        System.out.println(processor.formatWithCommas());
        System.out.println("Expected: 999,999");
        
    }
}
