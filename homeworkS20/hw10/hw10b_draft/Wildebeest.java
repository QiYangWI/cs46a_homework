//SOLUTION
/**
 * Models a wildebeest that can run and that eats when it sees food
 */
public class Wildebeest
{
    public static final int NOT_HUNGRY = 1;
    public static final int SOMEWHAT_HUNGRY = 2;
    public static final int HUNGRY = 3;
    public static final int VERY_HUNGRY = 4;

    private int state;

    /**
     * Constructs a Wildebeest that is very hungry.
     */
    public Wildebeest()
    {
        state = VERY_HUNGRY;
    }

    /**
     * Simulates the Wildebeest running. The Wildebeest 
     * becomes more hungry if not already VERY_HUNGRY
     */
    public void run()
    {
    }

    /**
     * Simulates eating. 
     * The  Wildebeest  will eat if it is hungry
     * and become less hungry
     */
    public void seeFood()
    {
    }

    /**
     * Gets the integer representing the state
     * @return the integer representing the state
     */
    public int getState()
    {
        return state;
    }

    /**
     * Gets a string describing the current hunger state of the Fish
     * @return a string describing the current hunger state of the Fish: "Not hungry", "Somewhat hungry ,"Hungry", or "Very hungry"
     */
    public String getHungerLevel() 
    {
        return null;
    }
}
