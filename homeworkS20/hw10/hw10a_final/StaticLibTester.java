import java.util.ArrayList;
/**
 * Tester for the static methods of StaticLib
 * 
 * @author Kathleen O'Brien
 */
public class StaticLibTester
{
    public static void main(String[] args)
    {
        //array max
        double[] numbers = {5.0, 6.6, 9.1, 8.4, 6.6,  7.4, 6.55};
        System.out.println("max array: "+ StaticLib.max(numbers));
        System.out.println("Expected: 9.1");
        double[] numbers2 = {-5.0, -9.3, -8.1, -6.6, -7.4, -10.2, -5.7, -13.3};
        System.out.println("max array: "+ StaticLib.max(numbers2));
        System.out.println("Expected: -5.0");
        double[] numbers3 = {};
        System.out.println("max array: "+ StaticLib.max(numbers3));
        System.out.println("Expected: -Infinity");
        
        ArrayList<Double> list = new ArrayList<>();
        //make an arraylist with same values as the array the lazy way
        for(double d : numbers)
        {
            list.add(d);
        }
        System.out.println("max arraylist: "+ StaticLib.max(list));
        System.out.println("Expected: 9.1");
        
        ArrayList<Double> list2 = new ArrayList<>();
        for(double d : numbers2)
        {
            list2.add(d);
        }
        System.out.println("max array: "+ StaticLib.max(list2));
        System.out.println("Expected: -5.0");
        
        ArrayList<Double> list3 = new ArrayList<>();
        System.out.println("max array: "+ StaticLib.max(list3));
        System.out.println("Expected: -Infinity");
        
       //count of times target appears in array    
       String[] strings = {"cat", "dog", "horse", "snake", "cat", "parrot",
           "cat", "pig", "cow", "horse"};
       System.out.println("in array: "
           + StaticLib.getCount(strings,"cat"));
       System.out.println("Expected: 3");   
       System.out.println("in array: "
           + StaticLib.getCount(strings,"dog"));
       System.out.println("Expected: 1");
       System.out.println("in array: "
           + StaticLib.getCount(strings,"hamster"));
       System.out.println("Expected: 0"); 
       
       //empty array
       strings = new String[0];
       System.out.println("in empty array: "
           + StaticLib.getCount(strings,"horse"));
       System.out.println("Expected: 0"); 
       
       //array list getCount 
       ArrayList<String> words = new ArrayList<>();
       words.add("cat");
       words.add("dog");        
       words.add("snake"); 
       words.add("cat");
       words.add("parrot");
       words.add("cat"); 
       words.add("cow");
       words.add("pig");
       words.add("horse");
       words.add("horse");
       
       System.out.println("in arraylist: "
           + StaticLib.getCount(words,"cat"));
       System.out.println("Expected: 3");   
       System.out.println("in arraylist: "
           + StaticLib.getCount(words,"dog"));
       System.out.println("Expected: 1");
       System.out.println("in arraylist: "
           + StaticLib.getCount(words,"hamster"));
       System.out.println("Expected: 0"); 
       
       //empty list
       words = new ArrayList<>();
       System.out.println("in empty arraylist: "
           + StaticLib.getCount(words,"horse"));
       System.out.println("Expected: 0"); 
    }
}
