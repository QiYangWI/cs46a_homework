//SOLUTION
import java.util.ArrayList;
/**
 * A Utility class containing static methods
 * for working with arrays and array lists
 */
public class StaticLib
{
    /**
     * Gets the average value in the array
     * @param numbers the array to find the average value of
     * @return the average value in the array or Double.NEGATIVE_INFINITY if 
     * the array is empty
     */
    public static double max(double[] numbers)
    {
        if (numbers.length == 0)
        {
            return Double.NEGATIVE_INFINITY;
        }
        
        double max = numbers[0];
        for (double n : numbers)
        {
            if (n > max)
            {
                max = n;
            }
        }
        return max;
    }

    /**
     * Gets the max value in the ArrayList
     * @param numbers ArrayList to process. 
     * @return the max value in the ArrayList or Double.NEGATIVE_INFINITY if the
     * array list is empty
     */
    public static double max(ArrayList<Double> numbers)
    {
        if (numbers.size() == 0)
        {
            return Double.NEGATIVE_INFINITY;
        }
        double max = numbers.get(0);
        for (double n : numbers)
        {
            if (n > max)
            {
                max = n;
            }
        }
        return max;
    }

    /**
     * Gets the number of times the target is in the array . 
     * @param list the array to process
     * @param target the value to search for
     * @return the number of times the target is in the array . 
     * 
     */
    public static int getCount(String[] list, String target)
    {
        //if you use the enhanced for loop this code,
        //it is exactly the same for the array and ArrayList
        int count = 0;
        for (String n : list)
        {
            if (n.equals( target))
            {
                count++;
            }
        }

        return count;      
    }

    /**
     * Gets the number of times the target is inthe ArrayList 
     * @param list the ArrayList to process
     * @param target the value to search for
     * @return Gets the number of times the target is inthe ArrayList
     */
    public static int getCount(ArrayList<String> list, String target)
    {
        //if you use the enhanced for loop this code,
        //it is exactly the same for the array and ArrayList
        int count = 0;
        for (String n : list)
        {
            if (n.equals( target))
            {
                count++;
            }
        }

        return count;      
    }

}
