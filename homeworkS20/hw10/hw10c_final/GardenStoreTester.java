import java.util.ArrayList;
/**
 * Tester for GardenStore
 */
public class GardenStoreTester
{
    public static void main(String[] args)
    {
        GardenStore plants = new GardenStore();
        plants.add(new Plant("dwarf maple tree", 121.75));
        plants.add(new Plant("blue star creeper flat", 15.95));
        plants.add(new Plant("marigold jumbo pack", 6.20));
        plants.add(new Plant("tomato plant", 4.50));
        plants.add(new Plant("lemon cucumber", 4.50));
        
        System.out.println(plants.plantList());
        System.out.println("Expected: [dwarf maple tree, blue star creeper flat, " 
          + "marigold jumbo pack, tomato plant, lemon cucumber]");
        
        System.out.println(plants.sum());
        System.out.println("Expected: 152.9");
        
        System.out.println(plants.contains("marigold jumbo pack"));
        System.out.println("Expected: true");
        System.out.println(plants.contains("sweet corn"));
        System.out.println("Expected: false");
        
        plants.add(new Plant("California poppy 6-pack", 6.00));
        
        System.out.println(plants.plantList());
        System.out.println("Expected: [dwarf maple tree, blue star creeper flat, " 
          + "marigold jumbo pack, tomato plant, lemon cucumber, California poppy 6-pack]");
        
        System.out.println(plants.sum());
        System.out.println("Expected: 158.9");
    }
}
