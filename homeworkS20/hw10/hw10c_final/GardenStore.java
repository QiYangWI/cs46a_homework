//SOLUTION
import java.util.ArrayList;
/**
 * Encapsulates an ArrayList of Plant objects
 */
public class GardenStore
{
    private ArrayList<Plant> list;

    /**
     * Constructs an emtpy GardenStore
     */
    public GardenStore()
    {
        list = new ArrayList<>();
    }

    /**
     * Adds a Plant to this GardenStore
     * @param plant the Plant to add
     */
    public void add(Plant plant)
    {
        list.add(plant);
    }
    
    /**
     * Gets an ArrayList of all the plant names
     * @return an ArrayList of all the plant names
     */
    public ArrayList<String> plantList()
    {
        ArrayList<String> results = new ArrayList<>();
        for (Plant p : list)
        {
            results.add(p.getName());    
        }
        return results;
    }
    
    /**
     * determines if a plant with a given name is in the GardenStore
     * @param name the name of the Plant to search for
     * @return true if a Plant with that name is in the 
     * inventory, else false.
     */
    public boolean contains(String name)
    {
        boolean found = false;
        int i = 0;
        if (list.size() != 0)
        {
            while (i < list.size() && !found)
            {
                Plant plant = list.get(i);
                if (plant.getName().equals(name))
                {
                    found = true;
                }
                else 
                {
                    i++;
                }
            }
        }
        return found;
    }
    
    /**
     * Gets the total cost of all the plants in the 
     * GardenStore
     * @return the total cost of all the plants in the 
     * GardenStore
     */
    public double sum()
    {
        double cost = 0;
        for (Plant p : list)
        {
            cost = cost + p.getPrice();
        }
        
        return cost;
    }
}
