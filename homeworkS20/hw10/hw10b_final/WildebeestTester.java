
/**
 * Tester for the Wildebeest class
 */
public class WildebeestTester
{
    public static void main(String[] args)
    {
        Wildebeest wildebeest = new Wildebeest();
        System.out.println("state: " + wildebeest.getState()
            + " " + wildebeest.getHungerLevel());
        System.out.println("Expected: 4 Very hungry");
        
        wildebeest.run();
        System.out.println("state: " + wildebeest.getState()
            + " " + wildebeest.getHungerLevel());
        System.out.println("Expected: 4 Very hungry");
        
        wildebeest.seeFood();
        System.out.println("state: " + wildebeest.getState()
            + " " + wildebeest.getHungerLevel());
        System.out.println("Expected: 3 Hungry");
        
        wildebeest.seeFood();
        System.out.println("state: " + wildebeest.getState()
            + " " + wildebeest.getHungerLevel());
        System.out.println("Expected: 2 Somewhat hungry");
        
        wildebeest.seeFood();
        System.out.println("state: " + wildebeest.getState()
            + " " + wildebeest.getHungerLevel());
        System.out.println("Expected: 1 Not hungry");
        
        wildebeest.seeFood();
        System.out.println("state: " + wildebeest.getState()
            + " " + wildebeest.getHungerLevel());
        System.out.println("Expected: 1 Not hungry");
        
        wildebeest.run();
        System.out.println("state: " + wildebeest.getState()
            + " " + wildebeest.getHungerLevel());
        System.out.println("Expected: 2 Somewhat hungry");
        
        wildebeest.run();
        System.out.println("state: " + wildebeest.getState()
            + " " + wildebeest.getHungerLevel());
        System.out.println("Expected: 3 Hungry");
        
        wildebeest.run();
        System.out.println("state: " + wildebeest.getState()
            + " " + wildebeest.getHungerLevel());
        System.out.println("Expected: 4  Very hungry");
        
        wildebeest.run();
        System.out.println("state: " + wildebeest.getState());
        System.out.println("Expected: 4");
                
    }
}
