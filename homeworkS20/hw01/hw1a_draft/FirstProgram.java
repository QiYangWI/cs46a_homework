//SOLUTION
/**
 * A program to do some printing in the println statement.
 * 
 * @author Kathlen O'Brien
 */
public class FirstProgram
{
   public static void main(String[] args)
   {
       
       //your code goes here
       
//HIDE
        System.out.println("If you can keep your head when all about you");
//SHOW
   }
}