//SOLUTION
/**
 * printing Unicode
 * 
 * @author Kathleen O'Brien 
 */
public class UnicodeForAll
{
    public static void main(String[] args)
    {
        //put your code here
//HIDE
        System.out.println("|*girl*|");
        System.out.println("|*ni\u00F1a*|");
        System.out.println("|*\uF8FF\u2602\u2603\uF8FF*|");
        
//SHOW
    }
}