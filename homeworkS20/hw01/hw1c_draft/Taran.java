//SOLUTION
//SHOW
/**
 * Display a Picture of Taran
 * @author Kathleen O'Brien
 */
public class Taran
{
    public static void main(String[] args)
    {
        //Your code goes here
//HIDE         
         Picture taran = new Picture("taran and family.jpg");        

         taran.draw();
//SHOW
    }
}