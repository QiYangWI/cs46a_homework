import java.util.Arrays;
/**
 * Tester for the Plant class Comparable
 */
public class PlantRunner
{
    public static void main(String[] args)
    {
   
        Plant plant1 = new Plant("blue star creeper flat", 15.95);
        Plant plant2 = new Plant("dwarf maple tree", 121.75);
        Plant plant3 = new Plant("marigold jumbo pack", 6.25);
        Plant plant4 = new Plant("crookneck squash", 6.25);        
        
        System.out.println(plant1.compareTo(plant2));
        System.out.println("Expected: -1"); 
        
        Plant[] plants = {plant1, plant2, plant3, plant4};
        
        Arrays.sort(plants);
        
        for (Plant p : plants)
        {
            System.out.println(p.getName() + " " + p.getPrice());
        }
        
        
       
    }
}
