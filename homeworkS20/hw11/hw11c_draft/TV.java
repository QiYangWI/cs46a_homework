//SOLUTION
//SHOW
/**
 * Models a television with a brand and a 
 * screen size in inches 
 */
public class TV 
{
     // your code here    

//HIDE
    private String brand;
    private double size;
    
    /**
     * Constructs a TV object
     * @param brand the manufacturer
     * @param size the size of this TV
     */
    public TV(String brand, double size)
    {
        this.brand = brand;
        this.size = size;
    }
    
    /**
     * Gets the brand of this TV
     * @return the brand of this TV object
     */
    public String getBrand()
    {
        return brand;
    }
    
    /**
     * Gets the clock speed of this TV
     * @return the size of this TV object
     */
    public double getSize()
    {
        return size;
    }
    
//SHOW
    /**
     * Gets a string representation of the object
     * @return a string representation of the object
     */
    public String toString()
    {
        String s = getClass().getName()
                + "[brand=" + brand
                + ",size=" + size 
                + "]";
        return s;
    }
}
