//SOLUTION
public class TreeRunner
{
    public static void main(String[] args)
    {
        Tree[] forest = new Tree[6];
        forest[0] = new Tree("avocado", 35.0);
        forest[1] = new Tree("dwarf maple tree", 4.0);
        forest[2] = new Tree("lemon",12.5 );
        forest[3] = new Tree("apple", 20.0);
        forest[4] = new Tree("cherry", 12.5);
        forest[5] = new Tree("apricot", 17);
          
        // Add the code to get the average height of the Trees 
        //between here and the comment that says end
        //HIDE
        System.out.println(Data.average(forest));
        //SHOW
        
        //end
        
        
        //
        // Add the code to get the type of tallest tree between the comments
        //
        //HIDE
        Measurable largest = Data.largest(forest);
        Tree largestTree = (Tree)largest; 
        System.out.println(largestTree.getType());
        //SHOW
        
        // end
        
        
        
       
    }
}
