//SOLUTION
//SHOW
/**
 * Calculates how many days until New Year's Eve 2019
 *
 * @author Kathleen O'Brien
 */

public class HowLongIsIt
{
    public static void main(String[] args)
    {
        //put your code here
        
        //HIDE
        Day today = new Day(); 
        //SHOW

        //do not change this line
        System.out.println("The date is " + today.toString());       
    }
}
