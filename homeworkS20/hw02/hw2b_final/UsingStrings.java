//SOLUTION
//IGNORECASE false
/**
 * Use some string methods
 * 
 * @author (your name) 
 */
public class UsingStrings
{
    public static void main(String[] args)
    {
        String word =  "leEt"; //SUB "ELite" 
        //do not change the line above here
        
        //add your code between here
        
//HIDE        
        System.out.println(word.length());
        System.out.println(word.toUpperCase());
  
        String lower = word.toLowerCase();
        System.out.println(lower.replace("e","3"));
        word = word.replace("t", "7");

//SHOW  
        //and here
        System.out.println("The word is: " + word); // do not change this line
        
      
    }
}
