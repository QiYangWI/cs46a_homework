//SOLUTION
//SHOW
/**
 * Calculates how many days until New Year's Eve 2019
 *
 * @author Kathleen O'Brien
 */

public class HowLongIsIt
{
    public static void main(String[] args)
    {
        //put your code here
        
        //HIDE
        Day today = new Day(); 
        
        Day newYearsEve = new Day(2019, 12, 31);
        System.out.println(newYearsEve.toString());
        int daysUntil = newYearsEve.daysFrom(today);
        System.out.println(daysUntil);
        Day todayPlus25 = today.addDays(25);
        System.out.println(todayPlus25.getYear());
        System.out.println(todayPlus25.getMonth());
        System.out.println(todayPlus25.getDate());
        //SHOW

        //do not change this line
        System.out.println("The date is " + today.toString());       
    }
}
