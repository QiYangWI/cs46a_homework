//SOLUTION
/**
 * Draws a simulation of a railroad crossing signal
 * @author your name
 */
public class RailroadCrossingSignal
{
    public static void main(String[] args)
    {
//HIDE
        Ellipse leftCircle = new Ellipse(50, 30, 40, 40);
        leftCircle.setColor(Color.RED);
        leftCircle.fill();
        
        Ellipse rightCircle = new Ellipse(50 + 40+ 20, 30, 40, 40);
        rightCircle.fill();
        
        
        Rectangle topBar = new Rectangle(70, 30, 60, 2);
        topBar.fill();
//SHOW
    }
}
