//SOLUTION
/**
 * Process Strins with loops
 *
 * @author (KOBrien
 * 
 */
public class StringsAndLoops
{
    private String text;
    
    /**
     * Constructs a StringsAndLoops object with the given text
     * @param theText the text for this object
     */
    public StringsAndLoops(String theText)
    {
        text = theText;
    }
    
    /**
     * Gets the text for this object
     * @return the text for this object
     */
    public String getText()
    {
        return text;
    }
    
    /**
     * Gets the number of uppercase letters in the text
     * @return the number of uppercase objects in this text
     */
    public int getUpperCaseCount()
    {
        int count = 0;
        for (int i = 0; i < text.length(); i++)
        {
            if (Character.isUpperCase(text.charAt(i)))
            {
                count++;
            }
        }
        return count;
        
    }
}
