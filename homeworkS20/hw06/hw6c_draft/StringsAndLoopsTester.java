
/**
 * Tester for StringsAndLoops.
 *
 * @author KOBrien
 * @version (a version number or a date)
 */
public class StringsAndLoopsTester
{
    public static void main(String[] arga)
    {
        StringsAndLoops phrase = new StringsAndLoops("Today IS  a good Day tO Code!");
        System.out.println(phrase.getText());
        System.out.println("Expected: Today IS  a good Day tO Code!");        
        System.out.println(phrase.getUpperCaseCount());
        System.out.println("Expected: 6");
        
        phrase = new StringsAndLoops("");
        System.out.println(phrase.getUpperCaseCount());
        System.out.println("Expected: 0");
        
        phrase = new StringsAndLoops("java is fun");
        System.out.println(phrase.getText());
        System.out.println("Expected: java is fun");
        System.out.println(phrase.getUpperCaseCount());
        System.out.println("Expected: 0");
    }
}
