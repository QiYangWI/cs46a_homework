//SOLUTION
import java.util.Scanner;
/**
 * Using loops.
 *
 * @author KOBrien
 * 
 */
public class LoopsAndMore
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int sum = 0;
        for (int i = 1; i <= 3;i++)
        {
            System.out.print("Enter an integer: ");
            sum = sum + scan.nextInt();
        }
        System.out.println(sum);
    }
}
