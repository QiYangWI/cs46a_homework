
/**
 * Tests HomeLoanProcessor class.
 *
 * @author KOBrien
 * 
 */
public class HomeLoanProcessorTester
{
    public static void main(String[] args)
    {
        HomeLoanProcessor loan = new HomeLoanProcessor(200000, 60000, 7 );
        System.out.println("Loan: " + loan.getLoanAmount());
        System.out.println("Expected: 200000.0");
        System.out.println("Income: " + loan.getAnnualIncome());
        System.out.println("Expected: 60000.0");
        System.out.println("Years: " + loan.getYearsAtCurrentAddress());
        System.out.println("Expected: 7");
        System.out.println("Granted: " + loan.loanGranted());
        System.out.println("Expected: false");
        
        loan.setAnnualIncome(-2);
        System.out.println("Loan: " + loan.getLoanAmount());
        System.out.println("Expected: 0.0");
        System.out.println("Income: " + loan.getAnnualIncome());
        System.out.println("Expected: 0.0");
        System.out.println("Years: " + loan.getYearsAtCurrentAddress());
        System.out.println("Expected: 0");
        System.out.println("Graned: " + loan.loanGranted());
        System.out.println("Expected: false");
        
        loan.setLoanAmount(300000);
        System.out.println("Loan: " + loan.getLoanAmount());
        System.out.println("Expected: 0.0");  //notice this is incorrect because the method is a stub. 
        
        loan = new HomeLoanProcessor(500000, 70000, 3 );
        System.out.println("Loan: " + loan.getLoanAmount());
        System.out.println("Expected: 500000.0");
        System.out.println("Income: " + loan.getAnnualIncome());
        System.out.println("Expected: 70000.0");        
        System.out.println("Years: " + loan.getYearsAtCurrentAddress());
        System.out.println("Expected: 3");
        
        
    }
}
