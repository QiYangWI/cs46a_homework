//SOLUTION
import java.util.Scanner;
/**
 * Using loops.
 *
 * @author KOBrien
 * 
 */
public class LoopsAndMore
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        int integer = 10;
        do
        {
            System.out.print("Enter an integer less than 10 ");
            integer = scan.nextInt();
        } while(integer >= 10);
        
        double d = -0.1;
        do
        {
            System.out.print("Enter a double between 1 (inclusive) and 50 (exclusive)" );
            d = scan.nextDouble();
        }while (d < 1 || d >= 50);
        
        System.out.println(d);
        double product = integer * d;
        System.out.println(product);
        System.out.println((int)product);
    }
}
