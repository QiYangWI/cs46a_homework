//SOLUTION
/**
 * Process Strins with loops
 *
 * @author (KOBrien
 * 
 */
public class StringsAndLoops
{
    private String text;
    
    /**
     * Constructs a StringsAndLoops object with the given text
     * @param theText the text for this object
     */
    public StringsAndLoops(String theText)
    {
        text = theText;
    }
    
    /**
     * Gets the text for this object
     * @return the text for this object
     */
    public String getText()
    {
        return text;
    }
    
    /**
     * Gets the number of uppercase letters in the text
     * @return the number of uppercase objects in this text
     */
    public int getUpperCaseCount()
    {
        int count = 0;
        for (int i = 0; i < text.length(); i++)
        {
            if (Character.isUpperCase(text.charAt(i)))
            {
                count++;
            }
        }
        return count;
        
    }
    
    /**
     * Gets the number of a's in the String, either upper or lower case.
     * @return  the number of a's in the String, either upper or lower case.
     */
    public int getACount()
    {
        int count = 0;
        String upperText = text.toUpperCase();
        for (int i = 0; i < text.length(); i++)
        {
            if (upperText.substring(i, i + 1).equals("A"))
            {
                
                count++;
            }
        }
        return count;
    }
    
    /**
     * Gets a string consisting of the first character of every word in the string. 
     * @return a string consisting of the first character of every word in the phrase, 
     * or the empty String if the string is empty. 
     */
    public String firsts()
    {
        if (text.isEmpty())
        {           
            return "";
        }
        
        String firstLetters = text.substring(0, 1);
        int spaceIndex = text.indexOf(" ");
        
        while (spaceIndex != -1)
        {
            firstLetters = firstLetters + text.substring(spaceIndex +1, spaceIndex + 2);
            spaceIndex = text.indexOf(" ", spaceIndex + 1);
        }
        return firstLetters;
    }
}
