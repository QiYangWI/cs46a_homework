//SOLUTION
/**
 * Describes a Food class
 */
public class Food extends Product
{
    public static final int MDR = 65;
    private double vitaminC;

    /**
     * Constructs a Food with a price and 
     * description and number of mg vitamin C
     * @param thePrice the price of this product
     * @param theDescription the description of this 
     * product
     * @param vitaminC the grams of carbohydrates
     */
    public Food(String theDescription, double thePrice, double vitaminC)
    {
        super(theDescription, thePrice);
        this.vitaminC = vitaminC;
    }

    /**
     * Gets the milligrams of vitamin C in this Food
     * @return the milligrams of vitamin C in this Food
     */
    public double getVitaminC()
    {
        return vitaminC;
    }

    /**
     * Get how many servings of this food are needed to get 
     * the minimum daily requirement
     * @return how many servings of this food are needed to get 
     * the minimum daily requirement
     */
    public double howMuch()
    {
       
        return 0;
    }
}
