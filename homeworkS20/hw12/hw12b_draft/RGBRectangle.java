//SOLUTION
/**
 * Describes a Rectangle that can be red, 
 * green, blue, or gray
 */
public class RGBRectangle extends Rectangle
{
    private String rgbColor;

    /**
     * Constructor for objects of class rgbRectangle
     * @param x the x xoordinate
     * @param y the y coordinate
     * @param w the width
     * @param h the height
     */
    public RGBRectangle(int x, int y, int w, int h)
    {
        super(x, y, w, h);
        rgbColor = "gray";
        super.setColor(Color.GRAY);
    }

    /**
     * Gets the rgb color of this rectangle
     * @return the rgb color of this rectangle
     */
    public String getRGBColor()
    {
        return rgbColor;
    }
    
    /**
     * Sets the rgb color of this rectangle
     * @param newColor the rgb color
     */
    public void setRGBColor(String newColor)
    {
    }
}
