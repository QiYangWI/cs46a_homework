//SOLUTION
/**
 * Models an animal 
 */
public class Animal
{
    private int energy;
    public static final int INITIAL_ENERGY = 1;

    /**
     * Constructs an Animal with 1 unit of energy
     */
    public Animal()
    {
        energy = INITIAL_ENERGY;
    }

    /**
     * The Animal eats and increases energy
     * @param amount the amount eaten. 
     */
    public void eat(int amount)
    {
        energy = energy + amount;    
    }

    /**
     * The animal moves and decreases energy
     * by the amount moved
     * @param amount the amount moved. 
     */
    public void move(int amount)
    {
        energy = energy - amount;
    }

    /**
     * Gets the energy level of the Animal
     * @return the energy level of the Animal
     */
    public int getEnergy()
    {
        return energy;
    }
}