
/**
 * Tests the WallpaperJob class.
 *
 * @author KOBrien
 * 
 */
public class WallpaperJobTester
{
    public static void main(String[] args)
    {
        //test some constants
        System.out.println("Sq inches per foot: "
            + WallpaperJob.SQ_INCHES_PER_SQ_FOOT);
        System.out.println("Expected:144");
        System.out.println("Door area in square inches: "
            + WallpaperJob.DOOR_HEIGHT_IN_INCHES * WallpaperJob.DOOR_WIDTH_IN_INCHES);
        System.out.println("Expected: 2560.0");
        
        //construct a WallpaperJob
        WallpaperJob job = new WallpaperJob(10.5, 9.0);
        
        System.out.println("Ceiling area: " + job.getWidth() * job.getLength());
        System.out.println("Expected: 94.5");
        
        job.setDimensions(12, 10);
        System.out.println("Ceiling area: " + job.getWidth() * job.getLength());
        System.out.println("Expected: 120.0");
    }
}
