//SOLUTION
import java.util.Scanner;
/**
 * Produces an invoice for a wallpaper job.
 *
 * @author KOBrien
 * 
 */
public class WallpaperJobInvoice
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the name of the client: " );
        String clientName = scan.nextLine();
        System.out.println("Wallpaper Pros" );
        System.out.println(clientName);
    }
}
