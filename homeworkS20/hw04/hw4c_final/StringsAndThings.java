//SOLUTION
//IGNORESPACE FALSE
import java.util.Scanner;
/**
 * A class to manipulate strings
 *
 * @author KOBrien
 * 
 */
public class StringsAndThings
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a few words: ");
        String phrase = scan.nextLine();
        System.out.println(phrase);
        
        //last letter
        System.out.println(phrase.substring(phrase.length() - 1)); 
        
        //first word
        int firstSpace = phrase.indexOf(" ");
        String firstWord = phrase.substring(0, firstSpace);
        System.out.println(firstWord);
        
        System.out.print("Enter a word, a space, and an integer: ");
        String word = scan.next();
        int theInt = scan.nextInt();
        
        System.out.print("Enter a double: ");
        double theDouble = scan.nextDouble();
        
        System.out.println(theInt + theDouble);
        System.out.println(word);       
    }
}
