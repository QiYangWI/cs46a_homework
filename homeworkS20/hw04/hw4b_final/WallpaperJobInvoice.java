//SOLUTION
import java.util.Scanner;
/**
 * Produces an invoice for a wallpaper job.
 *
 * @author KOBrien
 * 
 */
public class WallpaperJobInvoice
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the name of the client: " );
        String clientName = scan.nextLine();
        System.out.println("Wallpaper Pros" );
        System.out.println(clientName);
        
        System.out.print("Enter the length of the room: ");
        double length = scan.nextDouble();
        System.out.print("Enter the width of the room: ");
        double width = scan.nextDouble();
        
        WallpaperJob job = new WallpaperJob(length, width);
        
        System.out.println("Total cost: " + job.getJobCost());
        System.out.println(job.getJobCost());
    }
}
