
/**
 * Tests the WallpaperJob class.
 *
 * @author KOBrien
 * 
 */
public class WallpaperJobTester
{
    public static void main(String[] args)
    {
        //test some constants
        System.out.println("Sq inches per foot: "
            + WallpaperJob.INCHES_PER_FOOT);
        System.out.println("Expected:12");
        System.out.println("Cost of labor per sqare foot: "
            + WallpaperJob.LABOR_COST_PER_SQ_FOOT);
        System.out.println("Expected: 1.1");
        
        //construct a WallpaperJob
        WallpaperJob job = new WallpaperJob(10.5, 9.0);
        System.out.printf("Area: %.2f%n", job.getArea());
        System.out.println("Expected: 388.72");
        System.out.printf("Total paper cost: %.2f%n", job.getCostOfWallpaper());
        System.out.println("Expected: 209.41");
        System.out.printf("Total cost: %.2f%n", job.getJobCost());
        System.out.println("Expected: 637.01");       
        
        job.setDimensions(15, 12);
        System.out.printf("Area: %.2f%n", job.getArea());
        System.out.println("Expected: 594.22");
        System.out.printf("Total cost: %.2f%n", job.getJobCost());
        System.out.println("Expected: 973.76");

    }
}
