//SOLUTION
import java.util.Scanner;
/**
 * A class to manipulate strings
 *
 * @author KOBrien
 * 
 */
public class StringsAndThings
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a few words: ");
        String phrase = scan.nextLine();
        System.out.println(phrase);
        System.out.println(phrase.substring(phrase.length() - 1));
        
        
    }
}
